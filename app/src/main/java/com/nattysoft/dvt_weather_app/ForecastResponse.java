package com.nattysoft.dvt_weather_app;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ForecastResponse {

    @SerializedName("city")
    public City city;

    @SerializedName("cod")
    public String cod;

    @SerializedName("message")
    public float message;

    @SerializedName("cnt")
    public int cnt;

    @SerializedName("list")
    public ArrayList<Forecast_List> list = new ArrayList<Forecast_List>();
}

class City {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("coord")
    public City_Coord coord;
    @SerializedName("country")
    public String country;
}

class City_Coord {
    @SerializedName("lon")
    public float lon;
    @SerializedName("lat")
    public float lat;
}

class Forecast_List {
    @SerializedName("dt")
    public long dt;
    @SerializedName("main")
    public Item_Main main;
    @SerializedName("weather")
    public ArrayList<Item_Weather> weather = new ArrayList<Item_Weather>();
    @SerializedName("clouds")
    public Item_Clouds clouds;
    @SerializedName("wind")
    public Item_Wind wind;
    @SerializedName("rain")
    public Item_Rain rain;
    @SerializedName("sys")
    public Item_Sys sys;
    @SerializedName("dt_txt")
    public String dt_txt;

    //list item inner classes
    class Item_Main {
        @SerializedName("temp")
        public float temp;

        @SerializedName("temp_min")
        public float temp_min;

        @SerializedName("temp_max")
        public float temp_max;

        @SerializedName("pressure")
        public float pressure;

        @SerializedName("sea_level")
        public float sea_level;

        @SerializedName("grnd_level")
        public float grnd_level;

        @SerializedName("humidity")
        public float humidity;

        @SerializedName("temp_kf")
        public float temp_kf;
    }

    class Item_Weather {
        @SerializedName("id")
        public int id;
        @SerializedName("main")
        public String main;
        @SerializedName("description")
        public String description;
        @SerializedName("icon")
        public String icon;
    }

    private class Item_Clouds {
        @SerializedName("all")
        public float all;
    }

    private class Item_Wind{
        @SerializedName("speed")
        public float speed;
        @SerializedName("deg")
        public float deg;
    }

    private class Item_Rain {
        @SerializedName("3h")
        public float h3;
    }

    private class Item_Sys {
        @SerializedName("pod")
        public String pod;
    }
}