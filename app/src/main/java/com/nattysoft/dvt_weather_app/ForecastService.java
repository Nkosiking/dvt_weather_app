package com.nattysoft.dvt_weather_app;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ForecastService {
    @GET("data/2.5/forecast?")
    Call<ForecastResponse> getWeatherForecastData(@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String app_id, @Query("units") String units);
}
