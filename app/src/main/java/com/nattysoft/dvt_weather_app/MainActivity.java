package com.nattysoft.dvt_weather_app;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private ProgressBar progressBar;
    private ConstraintLayout container;

    private Location location;
    private TextView txt_current_temp;
    private TextView txt_current_description;
    private TextView txt_min;
    private TextView txt_current;
    private TextView txt_max;
    //the weather forecast views
    private TextView txt_day_0;
    private ImageView img_icon_0;
    private TextView txt_day_temp_0;
    private TextView txt_day_1;
    private ImageView img_icon_1;
    private TextView txt_day_temp_1;
    private TextView txt_day_2;
    private ImageView img_icon_2;
    private TextView txt_day_temp_2;
    private TextView txt_day_3;
    private ImageView img_icon_3;
    private TextView txt_day_temp_3;
    private TextView txt_day_4;
    private ImageView img_icon_4;
    private TextView txt_day_temp_4;

    private ScrollView mainLayout;
    private ImageView title_img;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // 5 seconds
    //lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    //open weather map url
    public static String BaseUrl = "http://api.openweathermap.org/";
    public static String AppId = "3cc91e699d4d5f3d7abcfe17f02f3730";
    public static String lat = "35";
    public static String lon = "139";
    private Resources.Theme theme;
    private Resources.Theme preferedeTheme;
    private String weatherDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide(); //hide the title bar
        transparentStatusAndNavigation();

        int theme = PreferenceManager.getDefaultSharedPreferences(this).getInt("ActivityTheme", R.id.forest);
        // Set this Activity's theme to the saved theme
        setTheme(theme);

        progressBar = findViewById(R.id.progressBar);
        container = findViewById(R.id.container);
        progressBar.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);

        txt_current_temp = findViewById(R.id.txt_current_temp);
        txt_current_description = findViewById(R.id.txt_current_description);
        txt_min = findViewById(R.id.txt_min);
        txt_current = findViewById(R.id.txt_current);
        txt_max = findViewById(R.id.txt_max);
        txt_day_0 = findViewById(R.id.txt_day_0);
        img_icon_0 = findViewById(R.id.img_icon_0);
        txt_day_temp_0 = findViewById(R.id.txt_day_temp_0);
        txt_day_1 = findViewById(R.id.txt_day_1);
        img_icon_1 = findViewById(R.id.img_icon_1);
        txt_day_temp_1 = findViewById(R.id.txt_day_temp_1);
        txt_day_2 = findViewById(R.id.txt_day_2);
        img_icon_2 = findViewById(R.id.img_icon_2);
        txt_day_temp_2 = findViewById(R.id.txt_day_temp_2);
        txt_day_3 = findViewById(R.id.txt_day_3);
        img_icon_3 = findViewById(R.id.img_icon_3);
        txt_day_temp_3 = findViewById(R.id.txt_day_temp_3);
        txt_day_4 = findViewById(R.id.txt_day_4);
        img_icon_4 = findViewById(R.id.img_icon_4);
        txt_day_temp_4 = findViewById(R.id.txt_day_temp_4);
        mainLayout = findViewById(R.id.main_layout);
        title_img = findViewById(R.id.title_img);
        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.
                        toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }else {
            // targetSdkVersion < Android M, we have to use PermissionChecker
            return PermissionChecker.checkSelfPermission(this, permission)
                    == PermissionChecker.PERMISSION_GRANTED;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            txt_current_temp.setText("You need to install Google Play Services to use the App properly");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null  &&  googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            getCurrentData();
        }

        startLocationUpdates();
    }

    private void startLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }

    //get current weather
    void getCurrentData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WeatherService service = retrofit.create(WeatherService.class);
        Call<WeatherResponse> call = service.getCurrentWeatherData(lat, lon, AppId, "metric");
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(@NonNull Call<WeatherResponse> call, @NonNull Response<WeatherResponse> response) {
                if (response.code() == 200) {
                    WeatherResponse weatherResponse = response.body();
                    assert weatherResponse != null;
                    int theme_int = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getInt("ActivityTheme", R.id.forest);
                    switch(weatherResponse.weather.get(0).main)
                    {
                        case "Clear":
                            weatherDesc="Clear";
                            if(theme_int == R.id.forest) {
                                title_img.setBackground(getDrawable(R.drawable.forest_sunny));
                            }else{
                                title_img.setBackground(getDrawable(R.drawable.sea_sunnypng));
                            }
                            mainLayout.setBackgroundColor(getResources().getColor(R.color.sunny));
                            txt_current_description.setText(getResources().getText(R.string.sunny));
                            break;
                        case "Clouds":
                            weatherDesc="Clouds";
                            if(theme_int == R.id.forest) {
                                title_img.setBackground(getDrawable(R.drawable.forest_cloudy));
                            }else{
                                title_img.setBackground(getDrawable(R.drawable.sea_cloudy));
                            }
                            mainLayout.setBackgroundColor(getResources().getColor(R.color.cloudy));
                            txt_current_description.setText(getResources().getText(R.string.cloudy));
                            break;
                        case "Rain":
                        default:
                            weatherDesc="Rain";
                            if(theme_int == R.id.forest) {
                                title_img.setBackground(getDrawable(R.drawable.forest_rainy));
                            }else{
                                title_img.setBackground(getDrawable(R.drawable.sea_rainy));
                            }
                            mainLayout.setBackgroundColor(getResources().getColor(R.color.rainy));
                            txt_current_description.setText(getResources().getText(R.string.rainy));
                            break;
                    }
                    txt_current_temp.setText(String.format("%.0f",weatherResponse.main.temp)+"°");
                    txt_min.setText(String.format("%.0f",weatherResponse.main.temp_min)+"°");
                    txt_current.setText(String.format("%.0f",weatherResponse.main.temp)+"°");
                    txt_max.setText(String.format("%.0f",weatherResponse.main.temp_max)+"°");

                    getForecastData();
                    progressBar.setVisibility(View.GONE);
                    container.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherResponse> call, @NonNull Throwable t) {
                //weatherData.setText(t.getMessage());
            }
        });
    }

    //get weather forecast
    void getForecastData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ForecastService service = retrofit.create(ForecastService.class);
        Call<ForecastResponse> call = service.getWeatherForecastData(lat, lon, AppId, "metric");
        call.enqueue(new Callback<ForecastResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForecastResponse> call, @NonNull Response<ForecastResponse> response) {
                if (response.code() == 200) {
                    ForecastResponse forecastResponse = response.body();
                    assert forecastResponse != null;
                    List<Forecast_List> list = forecastResponse.list;
                    Forecast_List.Item_Main item_main = null;
                    String dateString = "";
                    String nextDate;
                    boolean skipDate;
                    int j = 0; //variable to trace skipped results.
                    for (int i=3; i<list.size(); i++){//skip all results for today
                        nextDate = list.get(i).dt_txt.substring(0, list.get(i).dt_txt.indexOf(" "));
                        if(i==3 || !dateString.equalsIgnoreCase(nextDate)){
                            dateString = nextDate;
                            skipDate = false;
                        }else{//skip the hours on that day
                            skipDate = true;
                            dateString = nextDate;
                        }
                        if(!skipDate){
                            item_main = list.get(i).main;

                            DateFormat formatter;
                            Date date = null;
                            formatter = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                date = formatter.parse(dateString);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            switch(j) {
                                case 0:
                                    txt_day_0.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date));
                                    Forecast_List.Item_Weather item_weather = list.get(i).weather.get(0);
                                    switch (item_weather.main) {
                                        case "Clear":
                                            img_icon_0.setImageDrawable(getResources().getDrawable(R.drawable.clear));
                                            break;
                                        case "Clouds":
                                            img_icon_0.setImageDrawable(getResources().getDrawable(R.drawable.partlysunny));
                                            break;
                                        case "Rain":
                                        default:
                                            img_icon_0.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                                            break;
                                    }
                                    txt_day_temp_0.setText(String.format("%.0f",item_main.temp)+ "°");
                                    break;
                                case 1:
                                    txt_day_1.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date));
                                    Forecast_List.Item_Weather item_weather_1 = list.get(i).weather.get(0);
                                    switch (item_weather_1.main) {
                                        case "Clear":
                                            img_icon_1.setImageDrawable(getResources().getDrawable(R.drawable.clear));
                                            break;
                                        case "Clouds":
                                            img_icon_1.setImageDrawable(getResources().getDrawable(R.drawable.partlysunny));
                                            break;
                                        case "Rain":
                                        default:
                                            img_icon_1.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                                            break;
                                    }
                                    txt_day_temp_1.setText(String.format("%.0f",item_main.temp) + "°");
                                    break;
                                case 2:
                                    txt_day_2.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date));
                                    Forecast_List.Item_Weather item_weather_2 = list.get(i).weather.get(0);
                                    switch (item_weather_2.main) {
                                        case "Clear":
                                            img_icon_2.setImageDrawable(getResources().getDrawable(R.drawable.clear));
                                            break;
                                        case "Clouds":
                                            img_icon_2.setImageDrawable(getResources().getDrawable(R.drawable.partlysunny));
                                            break;
                                        case "Rain":
                                        default:
                                            img_icon_2.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                                            break;
                                    }
                                    txt_day_temp_2.setText(String.format("%.0f",item_main.temp) + "°");
                                    break;
                                case 3:
                                    txt_day_3.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date));
                                    Forecast_List.Item_Weather item_weather_3 = list.get(i).weather.get(0);
                                    switch (item_weather_3.main) {
                                        case "Clear":
                                            img_icon_3.setImageDrawable(getResources().getDrawable(R.drawable.clear));
                                            break;
                                        case "Clouds":
                                            img_icon_3.setImageDrawable(getResources().getDrawable(R.drawable.partlysunny));
                                            break;
                                        case "Rain":
                                        default:
                                            img_icon_3.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                                            break;
                                    }
                                    txt_day_temp_3.setText(String.format("%.0f",item_main.temp) + "°");
                                    break;
                                case 4:
                                    txt_day_4.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date));
                                    Forecast_List.Item_Weather item_weather_4 = list.get(i).weather.get(0);
                                    switch (item_weather_4.main) {
                                        case "Clear":
                                            img_icon_4.setImageDrawable(getResources().getDrawable(R.drawable.clear));
                                            break;
                                        case "Clouds":
                                            img_icon_4.setImageDrawable(getResources().getDrawable(R.drawable.partlysunny));
                                            break;
                                        case "Rain":
                                        default:
                                            img_icon_4.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                                            break;
                                    }
                                    txt_day_temp_4.setText(String.format("%.0f",item_main.temp) + "°");
                                    break;
                            }
                            j++;
                        }
                    }
                }


            }

            @Override
            public void onFailure(@NonNull Call<ForecastResponse> call, @NonNull Throwable t) {
                //forecastData.setText(t.getMessage());
            }
        });
    }

    private void transparentStatusAndNavigation() {
        //make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.forest:
                PreferenceManager
                        .getDefaultSharedPreferences(this)
                        .edit()
                        .putInt("ActivityTheme", R.id.forest)
                        .commit();
                switch (weatherDesc){
                    case "Clear":
                        title_img.setBackground(getDrawable(R.drawable.forest_sunny));
                        break;
                    case "Clouds":
                        title_img.setBackground(getDrawable(R.drawable.forest_cloudy));
                        break;
                    case "Rain":
                        title_img.setBackground(getDrawable(R.drawable.forest_rainy));
                        break;
                }
                return true;
            case R.id.sea:
                PreferenceManager
                        .getDefaultSharedPreferences(this)
                        .edit()
                        .putInt("ActivityTheme", R.id.sea)
                        .commit();
                switch (weatherDesc){
                    case "Clear":
                        title_img.setBackground(getDrawable(R.drawable.sea_sunnypng));
                        break;
                    case "Clouds":
                        title_img.setBackground(getDrawable(R.drawable.sea_cloudy));
                        break;
                    case "Rain":
                        title_img.setBackground(getDrawable(R.drawable.sea_rainy));
                        break;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
